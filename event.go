package drpc

// Event is the constant for an event, declared at
// https://discordapp.com/developers/docs/topics/rpc#commands-and-events-rpc-events
type Event string

const (
	// Ready is non-subscription event sent immediately after connecting, contains server information
	Ready Event = "READY"
	// Error is non-subscription event sent when there is an error, including command responses
	Error Event = "ERROR"
	// GuildStatus is sent when a subscribed server's state changes
	GuildStatus Event = "GUILD_STATUS"
	// GuildCreate is sent when a guild is created/joined on the client
	GuildCreate Event = "GUILD_CREATE"
	// ChannelCreate is sent when a channel is created/joined on the client
	ChannelCreate Event = "CHANNEL_CREATE"
	// VoiceChannelSelect is sent when the client joins a voice channel
	VoiceChannelSelect Event = "VOICE_CHANNEL_SELECT"
	// VoiceStateCreate is sent when a user joins a subscribed voice channel
	VoiceStateCreate Event = "VOICE_STATE_CREATE"
	// VoiceStateUpdate is sent when a user's voice state changes in a subscribed voice channel (mute, volume, etc.)
	VoiceStateUpdate Event = "VOICE_STATE_UPDATE"
	// VoiceStateDelete is sent when a user parts a subscribed voice channel
	VoiceStateDelete Event = "VOICE_STATE_DELETE"
	// VoiceSettingsUpdate is sent when the client's voice settings update
	VoiceSettingsUpdate Event = "VOICE_SETTINGS_UPDATE"
	// VoiceConnectionStatus is sent when the client's voice connection status changes
	VoiceConnectionStatus Event = "VOICE_CONNECTION_STATUS"
	// SpeakingStart is sent when a user in a subscribed voice channel speaks
	SpeakingStart Event = "SPEAKING_START"
	// SpeakingStop is sent when a user in a subscribed voice channel stops speaking
	SpeakingStop Event = "SPEAKING_STOP"
	// MessageCreate is sent when a message is created in a subscribed text channel
	MessageCreate Event = "MESSAGE_CREATE"
	// MessageUpdate is sent when a message is updated in a subscribed text channel
	MessageUpdate Event = "MESSAGE_UPDATE"
	// MessageDelete is sent when a message is deleted in a subscribed text channel
	MessageDelete Event = "MESSAGE_DELETE"
	// NotificationCreate is sent when the client receives a notification (mention or new message in eligible channels)
	NotificationCreate Event = "NOTIFICATION_CREATE"
	// CaptureShortcutChange is sent when the user presses a key during shortcut capturing
	CaptureShortcutChange Event = "CAPTURE_SHORTCUT_CHANGE"
	// ActivityJoin is sent when the user clicks a Rich Presence join invite in chat to join a game
	ActivityJoin Event = "ACTIVITY_JOIN"
	// ActivitySpectate is sent when the user clicks a Rich Presence spectate invite in chat to spectate a game
	ActivitySpectate Event = "ACTIVITY_SPECTATE"
	// ActivityJoinRequest is sent when the user receives a Rich Presence Ask to Join request
	ActivityJoinRequest Event = "ACTIVITY_JOIN_REQUEST"
)
