package drpc

// Command is the command constant declared at
// https://discordapp.com/developers/docs/topics/rpc#commands-and-events-rpc-commands
type Command string

const (
	// Dispatch is event dispatch
	Dispatch Command = "DISPATCH"
	// Authorize is used to authorize a new client with your app
	Authorize Command = "AUTHORIZE"
	// Authenticate is used to authenticate an existing client with your app
	Authenticate Command = "AUTHENTICATE"
	// GetGuild is used to retrieve guild information from the client
	GetGuild Command = "GET_GUILD"
	// GetGuilds is used to retrieve a list of guilds from the client
	GetGuilds Command = "GET_GUILDS"
	// GetChannel is used to retrieve channel information from the client
	GetChannel Command = "GET_CHANNEL"
	// GetChannels is used to retrieve a list of channels for a guild from the client
	GetChannels Command = "GET_CHANNELS"
	// Subscribe is used to subscribe to an RPC event
	Subscribe Command = "SUBSCRIBE"
	// Unsubscribe is used to unsubscribe from an RPC event
	Unsubscribe Command = "UNSUBSCRIBE"
	// SetUserVoiceSettings is used to change voice settings of users in voice channels
	SetUserVoiceSettings Command = "SET_USER_VOICE_SETTINGS"
	// SelectVoiceChannel is used to join or leave a voice channel, group dm, or dm
	SelectVoiceChannel Command = "SELECT_VOICE_CHANNEL"
	// GetSelectedVoiceChannel is used to get the current voice channel the client is in
	GetSelectedVoiceChannel Command = "GET_SELECTED_VOICE_CHANNEL"
	// SelectTextChannel is used to join or leave a text channel, group dm, or dm
	SelectTextChannel Command = "SELECT_TEXT_CHANNEL"
	// GetVoiceSettings is used to retrieve the client's voice settings
	GetVoiceSettings Command = "GET_VOICE_SETTINGS"
	// SetVoiceSettings is used to set the client's voice settings
	SetVoiceSettings Command = "SET_VOICE_SETTINGS"
	// CaptureShortcut is used to capture a keyboard shortcut entered by the user
	CaptureShortcut Command = "CAPTURE_SHORTCUT"
	// SetCertifiedDevices is used to send info about certified hardware devices
	SetCertifiedDevices Command = "SET_CERTIFIED_DEVICES"
	// SetActivity is used to update a user's Rich Presence
	SetActivity Command = "SET_ACTIVITY"
	// SendActivityJoinInvite is used to consent to a Rich Presence Ask to Join request
	SendActivityJoinInvite Command = "SEND_ACTIVITY_JOIN_INVITE"
	// CloseActivityRequest is used to reject a Rich Presence Ask to Join request
	CloseActivityRequest Command = "CLOSE_ACTIVITY_REQUEST"
)
