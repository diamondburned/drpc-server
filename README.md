# drpc-server

My attempts at reverse-engineering the RPC server of the Discord client.

## Motivation

I did not like the Electron client, but the idea of making an RPC serve- 

## Tested with

- [x] [discord.nvim](https://github.com/aurieh/discord.nvim)
- [x] VSCode

## Running

This package has a `cmd` CLI. To run this, do

```sh
cd cmd/drpc-server/
go build # Run once
./drpc-server "$TOKEN"
```

## Testing

The tests, if provided a `$TOKEN` environment variable, will try and connect to
Discord and open a test RPC for as long as SIGINT is not triggered. Without the
`$TOKEN` variable, the test would only do packet read tests and the socket test.

To fully test this with an external RPC application, the CLI provided in `cmd`
should be used.

## Contributing

When contributing, please include a `spew.Dump` of both the `recv` (received)
packet and the `send` (sent) packet. This would aid massively in debugging
and/or adding new features.

## Known bugs

- `(*Handshake).Version` could be either `string` or `int`, may need additional
	tweaks/workarounds.
- `SUBSCRIBE` events don't do anything yet. I still yet have an idea of what 
	this does.
