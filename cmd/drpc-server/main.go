package main

import (
	"log"
	"os"
	"os/signal"

	"github.com/diamondburned/discordgo"
	"gitlab.com/diamondburned/drpc-server"
)

func main() {
	log.SetFlags(0)

	if len(os.Args) != 2 {
		log.Fatalln("usage: " + os.Args[0] + " token")
	}

	log.Println("Logging in")

	// Create a new REST-only session
	d, err := discordgo.New(os.Args[1])
	if err != nil {
		log.Fatalln(err)
	}

	if err := d.Open(); err != nil {
		log.Fatalln(err)
	}

	defer d.Close()

	log.Println("Starting RPC in", drpc.IPCPath)

	drpc.WarnLogger = func(err error) {
		log.Println(err)
	}

	if err := drpc.Start(d); err != nil {
		log.Fatalln(err)
	}

	defer drpc.Close()

	go drpc.Listen()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig
}
