package drpc

import (
	"testing"
)

const handshakeID = `591470979251961856`

var handshake = &Handshake{
	Version:  1,
	ClientID: handshakeID,
}

func testHandshake(t *testing.T) {
	p, err := NewPacketFromBody(handshake)
	if err != nil {
		t.Fatal(err)
	}

	p.SetOP(OPHandshake)

	r := HandleHandshake(p)

	if r == nil {
		t.Fatal("HandleHandshake returned nil")
	}

	if r.err != nil {
		t.Fatal(r.err)
	}
}
