package drpc

import (
	"encoding/json"
	"fmt"

	"github.com/diamondburned/discordgo"
)

// ActivityArg is the structure that corresponds to https://discordapp.com/developers/docs/topics/rpc#setactivity
type ActivityArg struct {
	PID      int       `json:"pid"`
	Activity *Activity `json:"activity"`
}

// Activity is the activity structure documented at
// https://discordapp.com/developers/docs/topics/gateway#activity-object
type Activity struct {
	Details string `json:"details,omitempty"`
	State   string `json:"state,omitempty"`

	// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-assets
	Assets     *discordgo.Assets     `json:"assets,omitempty"`
	Timestamps *discordgo.TimeStamps `json:"timestamps,omitempty"`

	// Ignored, as it's not in *discordgo.Game
	// Party   *ActivityParty   `json:"party,omitempty"`
	// Secrets *ActivitySecrets `json:"secrets,omitempty"`
}

// ActivityReply is the data to reply with when a SET_ACTIVITY is received.
type ActivityReply struct {
	*Activity
	Name  string `json:"name"`
	AppID string `json:"application_id"`
}

/*
// ActivityParty contains information for the current party of the player.
// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-party
type ActivityParty struct {
	ID   string `json:"id"`
	Size [2]int `json:"size"`
}

// ActivitySecrets contains secrets for Rich Presence joining and spectating.
// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-secrets
type ActivitySecrets struct {
	Match    string `json:"match,omitempty"`
	Join     string `json:"join,omitempty"`
	Spectate string `json:"spectate,omitempty"`
}
*/

// HandleSetActivity handles the payload for SetActivity commands.
// https://discordapp.com/developers/docs/topics/rpc#setactivity
func HandleSetActivity(payload *Payload) *RespondPacket {
	if payload.Command != SetActivity {
		return NewErrorPacket(payload, fmt.Errorf(
			"not a set activity payload: %v",
			payload.Command,
		))
	}

	app, availAssets := GetState()
	if app == nil {
		return NewErrorPacket(payload, ErrNoApplication)
	}

	var aa ActivityArg

	if err := json.Unmarshal(payload.Args, &aa); err != nil {
		return NewErrorPacket(payload, err)
	}

	var data = discordgo.UpdateStatusData{
		Game: &discordgo.Game{
			Name:          app.Name,
			Type:          discordgo.GameTypeGame,
			Details:       aa.Activity.Details,
			State:         aa.Activity.State,
			ApplicationID: app.ID,
		},
	}

	if aa.Activity.Assets != nil {
		// Copy the assets over without the image IDs
		data.Game.Assets.LargeText = aa.Activity.Assets.LargeText
		data.Game.Assets.SmallText = aa.Activity.Assets.SmallText

		// Only add images that are in the asset list
		for _, a := range availAssets {
			if a.Name == aa.Activity.Assets.LargeImageID {
				data.Game.Assets.LargeImageID = a.ID
			}

			if a.Name == aa.Activity.Assets.SmallImageID {
				data.Game.Assets.SmallImageID = a.ID
			}
		}

		aa.Activity.Assets = &data.Game.Assets
	}

	if aa.Activity.Timestamps != nil {
		data.Game.TimeStamps = *aa.Activity.Timestamps
	}

	if err := session.UpdateStatusComplex(data); err != nil {
		return NewErrorPacket(payload, err)
	}

	rpl, err := payload.NewReplyPayload(&ActivityReply{
		Activity: aa.Activity,
		Name:     app.Name,
		AppID:    app.ID,
	})

	if err != nil {
		return NewErrorPacket(payload, err)
	}

	p, err := rpl.MarshalPayload()
	if err != nil {
		return NewErrorPacket(payload, err)
	}

	return p.AsResponse()
}

// Should be ran on Socket close
func resetActivity() error {
	if session == nil {
		return nil
	}

	ResetState()
	return session.UpdateStatusComplex(discordgo.UpdateStatusData{
		Game: nil,
	})
}
