module gitlab.com/diamondburned/drpc-server

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/diamondburned/discordgo v0.12.1-0.20190623062041-2fa9c3fc9037
	github.com/go-test/deep v1.0.1
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/sys v0.0.0-20190621203818-d432491b9138 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190621195816-6e04913cbbac // indirect
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce // indirect
)
