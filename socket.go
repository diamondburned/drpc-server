package drpc

import (
	"encoding/binary"
	"errors"
	"io"
	"net"
)

// IPCPath is populated on init
var IPCPath string

var sock net.Listener

var (
	// ErrSocketUninitialized is returned when Initialize is not called
	ErrSocketUninitialized = errors.New("Socket is uninitialized")

	// ErrSocketClosed is returned when the socket is already closed
	ErrSocketClosed = errors.New("Socket is already closed")

	// ErrSocketOpened is returned when the socket is already opened
	ErrSocketOpened = errors.New("Socket is already opened")

	// ErrConnClosed is returned when OP code 2 or Close is received
	ErrConnClosed = errors.New("Connection closed")
)

// ErrUnsupportedPlatform is returned on platforms where either a socket
// can't be made, possibly because it's unsupported or have yet to be.
var ErrUnsupportedPlatform = errors.New("Unsupported platform")

// Fallback createSocket function. Platform-specific functions that actually
// work are set in `init_*.go' files
var createSocket func() (net.Listener, error)

// initialize starts a new RPC server. This will throw an error if the socket
// is already opened.
func initialize() error {
	if sock != nil {
		return ErrSocketOpened
	}

	if createSocket == nil || IPCPath == "" {
		return ErrUnsupportedPlatform
	}

	l, err := createSocket()
	if err != nil {
		return err
	}

	sock = l
	return nil
}

// shorthand
var le = binary.LittleEndian

// error returned if all means fail
//    opcode(4) + len(4) + []byte("oops")
var errFail = []byte{1, 0, 0, 0, 4, 0, 0, 0, 111, 111, 112, 115}

// handler function. `spew.Dump' to be uncommented when contributing.
func handle(c net.Conn) {
	for {
		r, err := read(c)
		if err != nil {
			WarnLogger(err)
			if err, ok := err.(net.Error); ok && err.Temporary() {
				continue
			}

			//spew.Dump("recv", r)

			break
		}

		//spew.Dump("recv", r)

		resp := Handler(r)
		if resp.err != nil {
			WarnLogger(resp.err)
		}

		b, err := resp.Bytes()
		if err != nil {
			WarnLogger(err)

			// Fallback to that ridiculous "oops" response
			b = errFail
		}

		//spew.Dump("send", resp)

		if _, err := c.Write(b); err != nil {
			WarnLogger(err)
			break
		}
	}

	if err := resetActivity(); err != nil {
		WarnLogger(err)
	}

	if err := c.Close(); err != nil {
		WarnLogger(err)
	}
}

// all returned errors are non-fatal
func read(r io.Reader) (*Packet, error) {
	var OPcode, length int32

	if err := binary.Read(r, le, &OPcode); err != nil {
		return nil, err
	}

	if err := binary.Read(r, le, &length); err != nil {
		return nil, err
	}

	resp := &Packet{Length: length}

	switch OPcode {
	case int32(OPHandshake):
		resp.OPCode = OPHandshake
	case int32(OPFrame):
		resp.OPCode = OPFrame
	case int32(OPClose):
		return resp, ErrConnClosed
	default:
		return resp, ErrUnknownOpcode
	}

	resp.Body = make([]byte, length)
	if _, err := r.Read(resp.Body); err != nil {
		return nil, err
	}

	// A Handshake of {} indicates a connection close.
	if resp.OPCode == OPHandshake && string(resp.Body) == "{}" {
		return resp, ErrConnClosed
	}

	return resp, nil
}
