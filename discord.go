package drpc

import (
	"sync"

	"github.com/diamondburned/discordgo"
)

type applicationState struct {
	sync.RWMutex
	app    *discordgo.ApplicationRPC
	assets []*discordgo.Asset
}

var _RPCState applicationState

// ResetState resets the Application state.
func ResetState() {
	_RPCState.Lock()
	defer _RPCState.Unlock()

	_RPCState.app = nil
	_RPCState.assets = nil
}

// SetState sets the RPC application state.
func SetState(rpcID string) error {
	r, err := session.ApplicationRPC(rpcID)
	if err != nil {
		return err
	}

	assets, err := session.ApplicationAssets(rpcID)
	if err != nil {
		return err
	}

	_RPCState.Lock()
	defer _RPCState.Unlock()

	_RPCState.app = r
	_RPCState.assets = assets

	return nil
}

// GetState gets the current active state, which includes the application and
// the assets slice. The application returned might be nil, refer to
// ErrNoApplication.
func GetState() (*discordgo.ApplicationRPC, []*discordgo.Asset) {
	_RPCState.RLock()
	defer _RPCState.RUnlock()

	if _RPCState.app == nil {
		return nil, nil
	}

	var app = new(discordgo.ApplicationRPC)
	*app = *_RPCState.app

	var assets = make([]*discordgo.Asset, len(_RPCState.assets))
	copy(assets, _RPCState.assets)

	return app, assets
}
