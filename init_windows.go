// +build windows

package drpc

import (
	"net"

	npipe "gopkg.in/natefinch/npipe.v2"
)

func init() {
	IPCPath = `\\.\pipe\discord-ipc-0`
	createSocket = func() (net.Listener, error) {
		return npipe.Listen(IPCPath)
	}
}
