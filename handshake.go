package drpc

import (
	"encoding/json"
	"errors"

	"github.com/diamondburned/discordgo"
)

// ErrNoApplication is returned when a command that needs an application
// is called, but there is no application active. This should only happen
// when the RPC client forgets to initiate a Handshake.
var ErrNoApplication = errors.New("No active applications")

// ErrNotHandshake is returned when the Packet is not a valid handshake.
var ErrNotHandshake = errors.New("not a handshake")

// Handshake is received everytime an RPC client logs in.
type Handshake struct {
	Version  int    `json:"v"`
	ClientID string `json:"client_id,omitempty"` // recv only

	// Handshake reply only
	Config *HandshakeConfig `json:"config,omitempty"`
	User   *HandshakeUser   `json:"user,omitempty"`
}

// HandshakeConfig is used in the handshake reply
type HandshakeConfig struct {
	// Default: cdn.discordapp.com
	CDNHost string `json:"cdn_host"`
	// Default: //discordapp.com/api
	APIEndpoint string `json:"api_endpoint"`
	// Default: production
	Environment string `json:"environment"`
}

// HandshakeUser is used in the handshake reply
type HandshakeUser struct {
	ID            string `json:"id"`
	Username      string `json:"username"`
	Discriminator string `json:"discriminator"`
	Avatar        string `json:"avatar"`
	Bot           bool   `json:"bot"`
	Flags         int64  `json:"flags"`
	PremiumType   int64  `json:"premium_type"`
}

// HandleHandshake handles a handshake
func HandleHandshake(p *Packet) *RespondPacket {
	if p.OPCode != OPHandshake {
		return NewErrorPacket(nil, ErrNotHandshake)
	}

	var h Handshake
	if err := json.Unmarshal(p.Body, &h); err != nil {
		return NewErrorPacket(nil, err)
	}

	if err := SetState(h.ClientID); err != nil {
		return NewErrorPacket(nil, err)
	}

	var u *discordgo.User

	if session.State != nil && session.State.User != nil {
		u = session.State.User
	} else {
		user, err := session.User("@me")
		if err != nil {
			return NewErrorPacket(nil, err)
		}

		u = user
	}

	// Reply

	data := &Handshake{
		Version: h.Version,
		Config: &HandshakeConfig{
			CDNHost:     "cdn.discordapp.com",
			APIEndpoint: "//discordapp.com/api",
			Environment: "production",
		},
		User: &HandshakeUser{
			ID:            u.ID,
			Username:      u.Username,
			Discriminator: u.Discriminator,
			Avatar:        u.Avatar,
			Bot:           u.Bot,
			Flags:         int64(u.Flags),
			PremiumType:   int64(u.PremiumType),
		},
	}

	pl, err := NewPayloadWithData(data, false)
	if err != nil {
		return NewErrorPacket(nil, err)
	}

	pl.SetEvent(Ready)

	p, err = pl.MarshalPayload()
	if err != nil {
		return NewErrorPacket(nil, err)
	}

	return &RespondPacket{p, nil}
}
