package drpc

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"net"
)

// ErrUnknownRequest is returned when the OP code of the packet is unknown.
var ErrUnknownRequest = errors.New("unknown request opcode")

// Handlers is a map containing all handlers for incoming requests.
var Handlers = map[OPCode]func(*Packet) *RespondPacket{
	OPHandshake: HandleHandshake,
	OPFrame:     HandlePayload,
}

// Handler handles all types of responses. The returned Packet is used for
// replying.
func Handler(p *Packet) *RespondPacket {
	if f, ok := Handlers[p.OPCode]; ok {
		return f(p)
	}

	return NewErrorPacket(nil, ErrUnknownRequest)
}

// Packet is received from the client, always. From a client's perspective,
// this is a ResponseThis is not documented in the
// official docs.
type Packet struct {
	OPCode OPCode
	Length int32
	Body   []byte
}

// NewPacketFromBody crafts a new *Packet from a body of any type
func NewPacketFromBody(body interface{}) (*Packet, error) {
	json, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	request := &Packet{
		OPCode: OPFrame,
		Body:   json,
		Length: int32(len(json)),
	}

	return request, nil
}

// SetOP sets the Packet's OP code
func (r *Packet) SetOP(op OPCode) {
	r.OPCode = op
}

// AsResponse returns the Packet as a RespondPacket
func (r *Packet) AsResponse() *RespondPacket {
	return &RespondPacket{r, nil}
}

// SendFrame sends the *Packet frame to the socket
func (r *Packet) SendFrame(c net.Conn) error {
	b, err := r.Bytes()
	if err != nil {
		return err
	}

	// Set the right OP code
	r.SetOP(OPFrame)

	_, err = c.Write(b)
	return err
}

// Bytes returns the request crafted into Little Endian encoded bytes
func (r *Packet) Bytes() ([]byte, error) {
	b := &bytes.Buffer{}
	b.Grow(int(r.Length + 8*2))

	if err := binary.Write(b, le, int32(r.OPCode)); err != nil {
		return nil, err
	}

	if err := binary.Write(b, le, r.Length); err != nil {
		return nil, err
	}

	b.Write(r.Body)
	return b.Bytes(), nil
}
