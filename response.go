package drpc

import (
	"encoding/json"
	"fmt"
	"path/filepath"
	"runtime"
)

// RespondPacket wraps around Packet, used for responses.
type RespondPacket struct {
	*Packet
	err error
}

type errorBody struct {
	Code    int
	Message string
}

// NewErrorPacket crafts a new Payload for an error. The original Payload
// could be nil, perhaps in case the error is from the handshake.
func NewErrorPacket(original *Payload, originalErr error) (p *RespondPacket) {
	var payload = &Payload{
		Command: Dispatch,
	}

	if original != nil {
		payload.Command = original.Command
		payload.Nonce = original.Nonce
	}

	payload.Event = Error

	body := &errorBody{
		Code: 4007, Message: originalErr.Error(),
	}

	if _, fn, line, ok := runtime.Caller(1); ok {
		body.Message = fmt.Sprintf(
			"%s:%d %s", filepath.Base(fn), line, body.Message,
		)
	}

	json, err := json.Marshal(body)

	if err != nil || body == nil {
		json = []byte(jsonMarshalError)
	}

	payload.Data = json

	packet, err := payload.MarshalPayload()
	if err != nil {
		panic(err)
	}

	// TODO: Confirm, this might be false
	packet.OPCode = OPClose

	return &RespondPacket{
		Packet: packet,
		err:    originalErr,
	}
}
