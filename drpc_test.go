package drpc

import (
	"bytes"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"testing"

	"github.com/diamondburned/discordgo"
	"github.com/go-test/deep"
)

var osTests func(*testing.T)

func TestMain(t *testing.T) {
	println("Testing read functions")
	// Test the read() functions
	testReadHandshake(t)
	testReadSetActivity(t)

	switch runtime.GOOS {
	case "linux", "windows":
		println(strings.Title(runtime.GOOS), "detected, running the socket test")

		if err := initialize(); err != nil {
			t.Fatal(err)
		}

		if err := Close(); err != nil {
			t.Fatal(err)
		}
	}

	println("Authenticating into Discord")
	token := os.Getenv("TOKEN")
	if token == "" {
		return

		//t.Fatal("No token given. Set $TOKEN.")
	}

	// Create a new REST-only session
	d, err := discordgo.New(token)
	if err != nil {
		t.Fatal(err)
	}

	if err := d.Open(); err != nil {
		t.Fatal(err)
	}

	defer d.Close()

	// Test the Discord handlers
	testHandlers(t, d)

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)
	<-sig
}

func testHandlers(t *testing.T, ses *discordgo.Session) {
	session = ses

	println("Sending a mock handshake")
	// Mock a handshake request
	testHandshake(t)

	println("Sending a mock SET_ACTIVITY")
	// Mock a SET_ACTIVITY request
	testSetActivity(t)
}

func testReadHandshake(t *testing.T) {
	// Start reconstructing the handshake in binary
	p, err := NewPacketFromBody(handshake)
	if err != nil {
		t.Fatal(err)
	}

	b, err := p.Bytes()
	if err != nil {
		t.Fatal(err)
	}

	// Feed the reconstructed handshake into the read function
	testPacket, err := read(bytes.NewReader(b))
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(p, testPacket); diff != nil {
		t.Fatal(diff)
	}
}

func testReadSetActivity(t *testing.T) {
	// Start reconstructing the SET_ACTIVITY request
	p, err := getSetActivity(t).MarshalPayload()
	if err != nil {
		t.Fatal(err)
	}

	b, err := p.Bytes()
	if err != nil {
		t.Fatal(err)
	}

	// Feed the reconstructed the SET_ACTIVITY request into the read function
	testPacket, err := read(bytes.NewReader(b))
	if err != nil {
		t.Fatal(err)
	}

	if diff := deep.Equal(p, testPacket); diff != nil {
		t.Fatal(diff)
	}
}
