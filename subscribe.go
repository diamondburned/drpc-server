package drpc

// SubscribeReply is the data of the reply payload for a SUBSCRIBE command.
type SubscribeReply struct {
	Event Event `json:"evt"`
}

// HandleSubscribe does nothing internally, it returns a RespondPacket
// pretending things are working.
// TODO: Actually add handlers for this.
func HandleSubscribe(payload *Payload) *RespondPacket {
	rpl, err := payload.NewReplyPayload(&SubscribeReply{
		Event: payload.Event,
	})

	if err != nil {
		return NewErrorPacket(nil, err)
	}

	p, err := rpl.MarshalPayload()
	if err != nil {
		return NewErrorPacket(payload, err)
	}

	return p.AsResponse()
}
