// Package drpc is a small implementation of the Discord RPC server. The only
// supported commands are handshake and set activity.
//
// The general API structure could be put like so:
//    - Response: (opcode, length, body)
//        - Handshake (0, ?, *Handshake)
//        - Payload   (1, ?, *Payload): (command, args, nonce:rand)
//            - SetActivity (SET_ACTIVITY, *ActivityPayload)
package drpc

import (
	"math/rand"
	"net"
	"time"

	"github.com/diamondburned/discordgo"
)

var session *discordgo.Session

// WarnLogger is used for non-important Socket errors.
var WarnLogger = func(err error) {}

// Start the socket for RPC and connect to Discord. The session is
// assumed to have already been opened.
func Start(ses *discordgo.Session) error {
	// Seed to randomize the nonces
	rand.Seed(time.Now().UnixNano())

	if err := initialize(); err != nil {
		return err
	}

	session = ses
	return nil
}

// Listen listens to the incoming RPC events.
func Listen() {
	if sock == nil {
		return
	}

Main:
	for {
		c, err := sock.Accept()
		if err != nil {
			WarnLogger(err)
			if err, ok := err.(net.Error); ok && !err.Temporary() && !err.Timeout() {
				return
			}

			continue Main
		}

		// socket.go
		go handle(c)
	}
}

// Close closes the socket and cleans up.
func Close() error {
	if sock == nil {
		return ErrSocketClosed
	}

	if err := sock.Close(); err != nil {
		return err
	}

	sock = nil
	session = nil
	return nil
}
