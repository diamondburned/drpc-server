// +build !windows

package drpc

import (
	"net"
	"os"
	"path/filepath"
)

func init() {
	path := os.Getenv("XDG_RUNTIME_DIR")
	if path == "" {
		path = os.TempDir()
	}

	/*
		for i := 0; i < 10; i++ {
			ipcPath := filepath.Join(path, "discord-ipc-"+strconv.Itoa(i))
			if _, err := os.Stat(path); err != nil {
				log.Println(err)
				IPCPath = ipcPath
				break
			}
		}
	*/

	IPCPath = filepath.Join(path, "discord-ipc-0")
	if IPCPath == "" {
		return
	}

	// createSocket creates a Unix socket, defaulted in /tmp/discord-ipc-0
	createSocket = func() (net.Listener, error) {
		return net.Listen("unix", IPCPath)
	}
}
