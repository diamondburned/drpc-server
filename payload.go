package drpc

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
)

// returned instead of the actual JSON when marshalling a JSON for error fails.
const jsonMarshalError = `{
	"code":44007, "message":"Can't marshal JSON for error"
}`

// ErrInvalidPayload is returned when the received *Request has an unknown
// command, if no handlers are found for that command or if the payload
// is just invalid.
var ErrInvalidPayload = errors.New("invalid payload")

// CommandHandlers is a map containing all handlers that handle the
// appropriate args.
var CommandHandlers = map[Command]func(p *Payload) *RespondPacket{
	SetActivity: HandleSetActivity,
	Subscribe:   HandleSubscribe,
}

// HandlePayload tries to parse the Payload response, then call the
// appropriate handler. If no handlers are found, ErrUnknownPayload is
// returned.
func HandlePayload(packet *Packet) *RespondPacket {
	if packet.OPCode != OPFrame {
		return NewErrorPacket(nil, ErrInvalidPayload)
	}

	var p Payload
	if err := json.Unmarshal(packet.Body, &p); err != nil {
		return NewErrorPacket(nil, err)
	}

	p.packet = packet

	if f, ok := CommandHandlers[p.Command]; ok {
		return f(&p)
	}

	return NewErrorPacket(nil, ErrInvalidPayload)
}

// Payload is the payload the client sends. The payload structure is documented
// at https://discordapp.com/developers/docs/topics/rpc#payloads.
type Payload struct {
	Command Command `json:"cmd"`
	// In commands received from the client
	Args json.RawMessage `json:"args,omitempty"`
	// In responses sent to the client
	Data json.RawMessage `json:"data,omitempty"`
	// In subscribed events, errors, and (un)subscribing events
	Event Event `json:"evt"`
	// In responses to commands (not subscribed events)
	Nonce string `json:"nonce"`

	packet *Packet
}

// NewPayload creates a new *Payload
func NewPayload() *Payload {
	return &Payload{
		Nonce: getNonce(),
	}
}

// NewPayloadWithData makes a new Payload with a data. Nonce is optional, as
// you're not supposed to have a nonce on subscribed events, eg READY.
func NewPayloadWithData(data interface{}, nonce bool) (*Payload, error) {
	j, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	pl := &Payload{
		Data: j,
	}

	if nonce {
		pl.Nonce = getNonce()
	}

	return pl, nil
}

// NewReplyPayload crafts a new Payload using the body and existing Payload
// information such as Nonce and Command.
func (pl *Payload) NewReplyPayload(body interface{}) (*Payload, error) {
	p, err := NewPayloadWithData(body, false)
	if err != nil {
		return nil, err
	}

	p.Nonce = pl.Nonce
	//p.Event = pl.Event
	p.Command = pl.Command
	return p, nil
}

// SetEvent sets the event in the payload
func (pl *Payload) SetEvent(ev Event) {
	pl.Command = Dispatch
	pl.Event = ev
}

// MarshalPayload marshal back the Payload into a Packet
func (pl *Payload) MarshalPayload() (*Packet, error) {
	return NewPacketFromBody(pl)
}

// NewRespondPacketFromBody calls NewPacketFromBody, except when the function
// fails, the returned RespondPacket will be for the error instead of the body.
func (pl *Payload) NewRespondPacketFromBody(body interface{}) *RespondPacket {
	p, err := NewPacketFromBody(body)
	if err != nil {
		return NewErrorPacket(pl, err)
	}

	p.SetOP(OPFrame)

	return &RespondPacket{p, nil}
}

// getNonce returns a UUID
func getNonce() string {
	buf := make([]byte, 16)
	rand.Read(buf)
	buf[6] = (buf[6] & 0x0f) | 0x40

	return fmt.Sprintf("%x-%x-%x-%x-%x", buf[0:4], buf[4:6], buf[6:8], buf[8:10], buf[10:])
}
