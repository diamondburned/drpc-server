package drpc

import "errors"

// ErrUnknownOpcode is returned when the request has an unknown opcode.
var ErrUnknownOpcode = errors.New("unknown opcode")

// OPCode is the type for OP codes
type OPCode int32

const (
	// OPHandshake is the code received when a client is logging in
	OPHandshake OPCode = iota

	// OPFrame is the code received when the client sends something
	// that has to do with the Payload struct, I'm not sure
	OPFrame

	// OPClose is returned when the response indicates an error (?)
	OPClose

	// Unhandled OP codes
	OPPing
	OPPong
)
