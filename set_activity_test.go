package drpc

import (
	"encoding/json"
	"os"
	"runtime"
	"strings"
	"testing"

	"github.com/diamondburned/discordgo"
)

func getSetActivity(t *testing.T) *Payload {
	var activity = &ActivityArg{
		PID: os.Getpid(),
		Activity: &Activity{
			Details: "Running " + strings.Title(runtime.GOOS),
			State:   "Testing",
			Assets: &discordgo.Assets{
				LargeImageID: "tfw",
				LargeText:    "drpc-server test",
				SmallImageID: "",
				SmallText:    "",
			},
		},
	}

	json, err := json.Marshal(activity)
	if err != nil {
		t.Fatal(err)
	}

	return &Payload{
		Command: SetActivity,
		Args:    json,
		Nonce:   getNonce(),
	}
}

func testSetActivity(t *testing.T) {
	if r := HandleSetActivity(getSetActivity(t)); r.err != nil {
		t.Fatal(r.err)
	}
}
